.MODEL SMALL
.CODE
ORG 100h
tdata :
	lusername db 13,10,'username : $'
	lpassword db 13,10,'password : $'
	lditerima db 13,10,'diterima : $'
	lditolak db 13,10,'ditolak $'
proses :
	mov ah,09h
	lea dx,lusername 
	int 21h

	mov ah,0ah
	lea dx,lusername
	int 21h
	
	mov ah,09h
	lea dx,lpassword
	int 21h

	mov ah,0ah
	lea dx,lpassword
	int 21h

	lea si, lusername
	lea di, lpassword

	cld
	mov cx, 23
	rep cmpsb
	jne gagal

	mov ah, 09h
	lea dx, lditolak
	int 21h
	jmp proses
gagal:
	mov ah,09h
	lea dx,lditerima
	int 21h
	jmp exit
exit:
	int 20h
end tdata